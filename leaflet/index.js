var mymap = L.map('mapid').setView([51.505, -0.09], 13);


L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFudXRlamFlbiIsImEiOiJja20zcTM5cnEyZmIzMnByemZ3ODZyY25hIn0.q7c42yFGKEUon3fvowF12w', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoibWFudXRlamFlbiIsImEiOiJja20zcTM5cnEyZmIzMnByemZ3ODZyY25hIn0.q7c42yFGKEUon3fvowF12w'
}).addTo(mymap);
<?php
    
    function palabraFrecuencia($archivo='el_quijote.txt', $palabra){
        $fd = fopen($archivo, 'r');
        $count = 0;
		while (($contenido = fgets($fd)) !== false) {             
            $arrayPalabras = explode(' ', $contenido);
            for ($i = 0; $i < count($arrayPalabras); $i++) {
                if ($arrayPalabras[$i] === $palabra) {                    
                    $count =  $count + 1;
                }
            }
		}   
        fclose($fd);
        return $count;    
    } //function palabraFrecuencia
    
    function sustituirPalabra($archivo, $salida, $encontrarPalabra, $palabraNueva){
        $fdinput = fopen($archivo, 'r');
        $fdoutput = fopen($salida, 'w');
        while (($contenido = fgets($fdinput)) !== false) {             
            $arrayPalabras = explode(' ', $contenido);
            for ($i = 0; $i < count($arrayPalabras); $i++) {
                if ($arrayPalabras[$i] === $encontrarPalabra) {                    
                    $arrayPalabras[$i] = $palabraNueva                    ;
                }                
            } // for
            $linea = implode(' ', $arrayPalabras);
            fwrite($fdoutput, $linea);
        } //while
        fclose($fdinput);
        fclose($fdoutput);
    } // function sustituirPalabra

    $encontrarPalabra = $_POST["encontrarPalabra"];
    $palabraNueva = $_POST["palabraNueva"];
    sustituirPalabra($archivo='el_quijote.txt', $salida='el_quijote_revisado.txt', $encontrarPalabra=$encontrarPalabra, $palabraNueva=$palabraNueva);
    echo "<p>Archivo de salida $salida</p>"

?>    
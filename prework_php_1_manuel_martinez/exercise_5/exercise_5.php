<?php
    
    function crearDirectorio($dirPath){        
        print_r($dirPath);
        if (!file_exists($dirPath)) {
            mkdir($dirPath, 0777, true);
            echo "<p>Directorio $dirPath creado</p>";    
        }
        return $dirPath;
    }  

    /* Otra manera de trabajar con fechas-horas   
    $fechaHoraActual = getdate();    
    print_r($fechaHoraActual);
    $fechaHoraActualString = strval($fechaHoraActual["year"]) . strval($fechaHoraActual["mon"]) . strval($fechaHoraActual["mday"]) . '_' . strval($fechaHoraActual["hours"]) . strval($fechaHoraActual["minutes"]) . strval($fechaHoraActual["seconds"]);
    echo "<p>$fechaHoraActualString</p>"; */

    $fechaHoraActual = new DateTime();
    $fechaHoraActualString = $fechaHoraActual->format('Y-m-d_H-i-s');    
    echo "<p>$fechaHoraActualString</p>";    
    $dirPath = getcwd() . DIRECTORY_SEPARATOR . $fechaHoraActualString;
    $dirPath = crearDirectorio($dirPath=$dirPath);

    $archivo = 'el_quijote.txt';
    $archivoModificado = $dirPath . DIRECTORY_SEPARATOR . str_replace('.txt', '_modificado.txt', $archivo);
    copy($archivo, $archivoModificado);
    

?>    
<?php
    
    function palabraFrecuencia($archivo='el_quijote.txt', $palabra){
        $fd = fopen($archivo, 'r');
        $count = 0;
		while (($contenido = fgets($fd)) !== false) {             
            $arrayPalabras = explode(' ', $contenido);
            for ($i = 0; $i < count($arrayPalabras); $i++) {
                if ($arrayPalabras[$i] === $palabra) {                    
                    $count =  $count + 1;
                }
            }
		}   
        fclose($fd);
        return $count;    
    } //function palabraFrecuencia
    
    $palabra = $_POST["palabra"];     
    $frecuencia = palabraFrecuencia($archivo='el_quijote.txt', $palabra=$palabra);
    echo "<br><p>La palabra '$palabra' aparece $frecuencia veces en la novela del Quijote</p>";
  
?>    
<?php
    
    function chequearContraseña($contraseña, $contraseñaConfirmacion){                        
        if (strlen($contraseña) < 8 ){
            echo "<p>La contraseña ha de contener al menos 8 caracteres</p>" ;
            return False;
        } elseif ($contraseña != $contraseñaConfirmacion) {
            echo "<p>La contraseña ha de ser igual a la confirmación</p>" ;
            return False;
        } else {
            echo "<p>Registrado correctamente</p>" ;
            return True;
        }                    
    }

    function chequearEmail($email){        
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Email invalido";
            return False;
        } else {
            return True;
        }
    }


    $email = $_POST["email"];
    $contraseña = $_POST["contraseña"];
    $confirmarContraseña = $_POST["confirmarContraseña"];
   
    if (chequearEmail($email)===False) {
        print_r("Email incorrecto");
    } elseif (chequearContraseña($contraseña, $confirmarContraseña)===False) {
        print_r("Contraseña incorrecta");
    }
    
?>    
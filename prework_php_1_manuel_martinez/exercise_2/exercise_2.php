<?php
    
    function compruebaVocales($frase){
        $vocales =["a", "e", "i", "o", "u"];
        $vocalesControl= [];
        $control = False;
        for ($i = 0; $i < strlen($frase); $i++) { 
            if (in_array($frase[$i], $vocales) && in_array($frase[$i], $vocalesControl)===False){
                array_push($vocalesControl, $frase[$i]);
                if (count($vocalesControl)===5){
                    $control = True;
                    break;
                }
            } //if
        } //for     
        return $control;
    }//function compruebaVocales
    

    $frase = strtolower($_POST["frase"]);     
    $control = compruebaVocales($frase=$frase);
    if ($control){
        echo '<p>LA PALABRA CONTIENE LAS 5 VOCALES</p>';
    } else {
        echo '<p>NO CONTIENE TODAS LAS VOCALES</p>';
    }
?>    
function compruebaValidez(dniNumber) {
  if (dniNumber < 0 || dniNumber > 99999999 || isNaN(dniNumber)) {
    console.log("Número incorrecto, inserte un número válido");
    return false;
  } else {
    return true;
  }
}

function obtenerLetraDni(dniNumber) {
  var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
  var resto = dniNumber % 23;
  return letras[resto];
}

var dniNumber = window.prompt('Ejercicio 3. Introduce el número de tu DNI:', '')
dniNumber = Number(dniNumber);

validez = compruebaValidez(dniNumber);
if (validez === true) {
  letra = obtenerLetraDni(dniNumber);
  alert("Tu DNI completo es " + dniNumber + "-" + letra);
}



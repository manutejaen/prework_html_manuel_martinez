console.log('mi primera ejecución con Node');

var num1 = 5;
var num2 = 10;

// asignación
num1 = num2; 
console.log(num1); // num1 pasa a valer 10

// asignación y suma (num1 ya no vale el 5 inicial, recordar que ahora vale 10)
num1 += num2;
console.log(num1); // num1 pasa a ser 20 

// asignación y resta
num1 -= num2;
console.log(num1); // num1 pasa a ser 10

// asignación y multiplicación
num1 *= num2;
console.log(num1); // num1 pasa a ser 100

// asignación y división
num1 /= num2;
console.log(num1); // num1 pasa a ser 10

var value = "Hola me llamo Jose y tengo 2 hijas";
console.log(value);
// Imprimirá: Hola me llamo Jose y tengo 2 hijas

var nombreHijas = ': Maria y Ana';
console.log(value + nombreHijas);
// Imprimirá: Hola me llamo Jose y tengo 2 hijas: Maria y Ana

var jediName = "Luke";
console.log(jediName[0]); // => L
console.log(jediName[3]); // => e
console.log(jediName[8]); // => undefined
console.log(jediName[-1]); // => undefined
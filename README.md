# Ejercicios Pre-Work BOOTCAMP UPGRADE HUB:

Código correspondiente a los tutoriales y ejercicios propuestos en el prework del Bootcamp de Upgrade Hub.

Dentro se encuentran las carpetas:
1. prework_css_1_manuel_martinez
    - Tutoriales
    - Primera página ***full responsive*** (Apple Page)
2. prework_html_1_manuel_martinez
3. prework_js_1_manuel_martinez
4. prework_project_CV_manuel_martinez
